# selectivityDeterminants

Implementation of the selectivity/specificity determinants calculation introduced in [Mirny&Gelfand2002](https://www.sciencedirect.com/science/article/abs/pii/S0022283602005879?via%3Dihub). 