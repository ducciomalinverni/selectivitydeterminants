#!/usr/bin/env python3

import sys
import scipy
import numpy as np
import sequenceHandler as sh
import matplotlib.pyplot as plt

def MI(x,y):

    x=x.ravel()
    y=y.ravel()
    
    nY=len(np.unique(y))
    bins=[21,nY]
    rng=[[-0.5,20.5],[-0.5,nY-0.5]]

    pxy,bx,by=np.histogram2d(x,y,bins=bins,range=rng,density=True)
    px = pxy.sum(axis=1)
    py = pxy.sum(axis=0)
    Mis = pxy[pxy>0]*np.log(pxy[pxy>0]/np.outer(px,py)[pxy>0])
    return  Mis.sum()

def getShuffledIs(x,y,nShuff):

    Is=np.zeros(nShuff)
    for i in np.arange(nShuff):
        xr=np.random.permutation(x)
        Is[i]=MI(xr,y)

    return Is

def fitCorrection(I,Ish):
    fit=scipy.stats.linregress(Ish[I>0.5,:].mean(axis=1),I[I>0.5])
    return fit[0],fit[1]

if __name__=="__main__":

    msaNames=sys.argv[1:-1]
    nShuff=int(sys.argv[-1])

    #Load ortholog msas and build master MSA and paralog tags vector
    print("Loading data")
    msas=[]
    for msaName in msaNames:
        msa,_=sh.fastaToMatrix(msaName)
        msas.append(msa)

    paralogTags = np.vstack([i*np.ones((ar.shape[0],1)) for i,ar in enumerate(msas)])
    msa = np.vstack(msas)
    Npos=msa.shape[1]

    # Compute mutual information and random shuffling for all positions
    print("Computing MI and ",nShuff,"shufflings")
    I=np.zeros(Npos)
    Ish=np.zeros((Npos,nShuff))
    for i in np.arange(Npos):
        I[i]=MI(msa[:,i],paralogTags)
        Ish[i,:]=getShuffledIs(msa[:,i],paralogTags,nShuff)
        if not i%100:
            print(i)
    # Compute correction and expected MI
    print("Computing linear correction and P-values")
    a,b = fitCorrection(I,Ish)
    Iexp = Ish*a +b
    mIexp=Iexp.mean(axis=1)
    sIexp=Iexp.std(axis=1)

    # Compute p-value of observed MI: Pval = erfc(z)
    z = (I-mIexp)/sIexp
    Ps = scipy.stats.norm.sf(z)
    np.savetxt('mirny_Nshuff'+str(nShuff)+'.dat',np.vstack((I,Ps.T,mIexp,sIexp)).T)

    # Plot the results

    plt.figure(figsize=(8,8))
    plt.subplot(2,1,1)
    plt.plot(np.arange(len(I))+1,I,c='k',linewidth=.75)
    plt.plot(np.arange(len(I))+1,mIexp,c='r',linewidth=.5)
    plt.plot(np.arange(len(I))+1,mIexp+sIexp,'--',c='r',linewidth=.5)
    plt.plot(np.arange(len(I))+1,mIexp-sIexp,'--',c='r',linewidth=.5)
    plt.legend(['Data','Shuffled'])
    plt.xlabel('Position')
    plt.ylabel('I')
    plt.grid(alpha=.3)
    plt.xlim([0,len(I)])
    
    plt.subplot(2,1,2)
    plt.semilogy(np.arange(len(I))+1,Ps,c='k',linewidth=.75)
    plt.xlabel('Position')
    plt.ylabel('P')
    plt.grid(alpha=.3)
    plt.xlim([0,len(I)])
    plt.savefig('mirni_Nshuff'+str(nShuff)+'.pdf')
